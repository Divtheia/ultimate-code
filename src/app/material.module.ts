import { NgModule } from '@angular/core';
import { MatInputModule, MatFormFieldModule, MatGridListModule, MatIconModule, MatButtonModule } from '@angular/material';


@NgModule({
    imports: [MatInputModule, MatFormFieldModule, MatGridListModule, MatIconModule, MatButtonModule], // 你想用的component
    exports: [MatInputModule, MatFormFieldModule, MatGridListModule, MatIconModule, MatButtonModule]  // 也必須將同樣的component或出
})
export class MaterialModule {

}
