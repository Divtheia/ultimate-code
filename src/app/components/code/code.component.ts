import { Component, OnInit } from '@angular/core';
import { trigger, transition, useAnimation } from '@angular/animations';
import { bounceInDown } from 'ng-animate';

@Component({
  selector: 'app-code',
  templateUrl: './code.component.html',
  styleUrls: ['./code.component.css'],
  animations: [
    trigger('bounceInDown', [transition('* => *', useAnimation(bounceInDown))]),
  ],
})
export class CodeComponent implements OnInit {
  // 設定答案
  answer = 653;

  // 設定最大值與最小值
  maxValue = 1000;
  minValue = 0;

  // input 數字
  inputValue: number;



  // 顯示設定
  show = false;
  // 動畫設定
  bounceInDown: any;

  constructor() { }

  ngOnInit() {
  }

  over() {
    if (this.inputValue > this.minValue && this.inputValue < this.maxValue) {
      return;
    } else {
      alert('請輸入範圍內的數字哦！');
      this.inputValue = null;
    }
  }


  onClick() {
    if (this.inputValue > this.minValue && this.inputValue < this.maxValue) {
      if (this.inputValue != this.answer) {
        if (this.inputValue > this.answer) {
          this.maxValue = this.inputValue;
          this.inputValue = null;
        } else {
          this.minValue = this.inputValue;
          this.inputValue = null;
        }
      } else {
        console.log('賓果');
        this.show = !this.show;
      }
    } else {
      console.log('超過囉!');
      this.over();
    }
  }
}
